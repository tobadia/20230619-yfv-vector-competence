# Vector competence of _Ae. aegypti_ and _Ae. albopictus_ for YFV
This repository holds all code used for the statistican analyses of the manuscript "Evaluating vector competence for Yellow fever in the Caribbean" by Gabiane _et al._ If I omit in the future, can someone please remind me to add the DOI here ?

## Content of the repository
The analyses are provided in a comprehensive RMarkdown report that can be re-run for convenience. The repository is organized as follows: 
* `data/`: contains the Excel spreadsheet with the raw vector competence data.
* `graphs/`: directory in which all graphs generated in the RMarkdown report are stored (when the corresponding code chunk is evaluated/ran).
* `images/`: directory containing images included in this README.md.
* `results/`: directory containing pre-calculated samples from the posterior for the Bayesian modelling, in a `.RData` format. The code chunks that actually _run_ the models in the RMarkdown reports are **not** evaluated (they include an `eval = FALSE` statement in their header). They can take a while to run, and therefore their results were stored and are loaded into R by the code chunks.
* `.gitignore`: to avoid syncing useless files into GitLab.
* `20230619-Failloux_Anna-Bella.Rmd`: the RMarkdown report file.
* `20230619-Failloux_Anna-Bella.Rproj`: the RStudio project file.
* `20230619-Failloux_Anna-Bella.html`: the knitted version of the RMarkdown report.

## Analyses conducted
The RMarkdown report should be comprehensive enough, but let's just go quickly through everything thatis covered in there.

First, the requirements for the R environment are loaded and the source data imported from the Excel spreadsheet. The rest report is split between the two studied species (_Ae. aegypti_ and _Ae. albopictus_).

![Number of dissected mosquitoes](/images/git_fig1.png)
_Albopictus_ mosquitoes have way less data and there is a near-complete plan for _aegypti_ at 14 and 21 days post-infection: the latter will be of key interest.

The key focus of the article is to characterize if, and to what extent, YFV from different genotypes are able to progress inside the body of mosquitoes from different species and populations (_i.e._ geographic sampling). As such, we define three events, that relate to biological barriers inside the mosquito:
1. Infection: detection of viral particles in mosquito midguts,
2. Dissemination: detection of viral particles in mosquito carcass,
3. Transmission: detection of viral particles in mosquito salivary glands.

Two sets of outcomes are derived: stepwise (the denominator is the number of successes from the preceding compartment) and cumulative (the denominator is always the total number of mosquitoes tested).

### Viral loads in mosquito compartments
The viral load is plotted and studied to try and distinguish patterns by YFV genotype, moquito population and days post-infection. They are also used in logistic regression models, to inform in the probability of dissemination to the next compartment.

### Bayesian modelling for the cumulative transmission rate
The CTR can be thought as the product of three probabilities: 

$$CTR = P(Y_{inf} = 1) * P(Y_{dis} = 1 | Y_{inf} = 1) * P(Y_{tra} = 1 | Y_{inf} = 1, Y_{dis} = 1)$$

This is actually the product of three binomial processes, where each time the population of interest is that that experienced a success at the preceding compartment. This was modeled in Stan, with models of increasing complexity to ultimately incoporate:
* $\mu$: intercept for logistic regressions
* $\beta_{dpi}$: the number of days post-infection \[categorical\]
* $\beta_{p}$: the mosquito population, _i.e._ the location where it was sampled before it was bred \[categorical\]
* $\beta_{g}$: the YFV genotype used in infection experiments \[categorical\]
* $\beta_{gp}$: an interaction term between the former two \[categorical\]
* $\beta_{v}$: the log10-transformed viral load in the preceding compartment \[continous\] (note: this term only exists for $Y_{dis}$ and $Y_{tra}$ since the infections for $Y_{inf}$ were conducted using blood meals at a fixed viral load)

There are three versions of the model, each time in non-vectorized then vectorized form (the latter being much more efficient in terms of computational time):
1. __M1: No intercept for any covariate, categorical predictores considered by covariate, no interaction terms__. This was useful to rememebr how to write in Stan again and to get one coefficient per categorical predictor, but overall this was not a good idea with possible identifiability issues,
2. __M2: Intercepts included, all categorical predictors into a single model matrix, no inteaction terms__,
3. __M3: Intercepts included, all categorical predictors into a single model matrix, inteaction terms included__.
